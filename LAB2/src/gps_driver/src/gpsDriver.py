#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
import serial
import math
import traceback
from gps_driver.msg import GPSUTM
import utm

class GPS_Driver:
    def __init__(self):
        try:

            self.gps_pub_topic = "/gps_utm"
            self.usb_port = "/dev/ttyUSB0"
            self.nmea_id = "$GPGGA"
            self.baud_rate = 4800
            rospy.init_node("gps_driver")
            self.gps_pub = rospy.Publisher(
                self.gps_pub_topic, GPSUTM, queue_size=10)
            self.serial_connection = serial.Serial(
                self.usb_port, self.baud_rate, timeout=2)
            self.gps_msg = GPSUTM()
            self.gps_stream_handler()

        except Exception as e:
            error = traceback.format_exc()
            rospy.logerr("ERROR: %s" % error)

    def gps_stream_handler(self):
        rospy.loginfo("Starting gps stream")
        # loop unil package is shutdown
        while not rospy.is_shutdown():
            
            # read serial data
            line = self.serial_connection.readline()
            data = line.split(b',')

            if data[0].decode("utf-8")  == self.nmea_id and (int(data[6].decode("utf-8")) == 4 or int(data[6].decode("utf-8")) == 5):  # $GPGGA 
                
                #nmea_time = float(data[1].decode("utf-8"))  # time from GPS
                lat = float(data[2].decode("utf-8"))/100
               
                lon = float(data[4].decode("utf-8"))/100

                altitude = float(data[9].decode("utf-8"))

                fix_quality = int(data[6].decode("utf-8"))

                self.gps_msg.utm_easting, self.gps_msg.utm_northing, self.gps_msg.zone, self.gps_msg.letter = utm.from_latlon(lat, lon)

                self.gps_msg.latitude = lat
                self.gps_msg.longitude = lon
                self.gps_msg.altitude = altitude
                self.gps_msg.fix_quality = fix_quality
                

                rospy.loginfo("Latitude: %f  Longitude: %f Altitude: %f UTM_Northing: %f UTM_Easting: %f Fix Qualtiy: %d" % (self.gps_msg.latitude, self.gps_msg.longitude, self.gps_msg.altitude, self.gps_msg.utm_easting, self.gps_msg.utm_northing, self.gps_msg.fix_quality))
                self.gps_pub.publish(self.gps_msg)
  


if __name__ == "__main__":

    gps_driver = GPS_Driver()
