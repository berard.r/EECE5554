import rospy
from bagpy import bagreader
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from lab1.msg import GPSUTM
from sensor_msgs.msg import NavSatFix
import math



if __name__ == "__main__":

    bag_file_path = './stationaryisec.bag'
    gps_topic = '/gps/gps'
    bag = bagreader(bag_file_path)

    data = bag.message_by_topic(gps_topic)
    gps_df = pd.read_csv(data)
    print(pd.unique(gps_df['lat']))
    
    raw_utm_easting = gps_df['easting']
    raw_utm_northing = gps_df['northing']
    raw_altitude = gps_df['alt']
    fix_quality = gps_df['fix']
    shifted_utm_easting = raw_utm_easting - raw_utm_easting[0]
    shifted_utm_northing  = raw_utm_northing - raw_utm_northing[0]
    shifted_altitude = raw_altitude# - raw_altitude[0]

    #get mean lat lon alt

    shifted_utm_easting_mean = shifted_utm_easting.mean()
    shifted_utm_northing_mean = shifted_utm_northing.mean()
    shifted_altitude_mean = raw_altitude.mean()

    distance_arr = []
    max_distance = 0
    min_distance = 0
    for idx, val in shifted_utm_easting.items():
        
        distance = math.sqrt((shifted_utm_easting[idx] - shifted_utm_easting_mean)**2 + (shifted_utm_northing[idx] - shifted_utm_northing_mean)**2)
        distance_arr.append(distance)
        
        

    distance_arr = np.asarray(distance_arr)
    average_distance_from_mean = distance_arr.mean()
    max_distance = distance_arr.max()
    
    print("Percentage of Points with Fix: %f" % ((fix_quality == 4).sum()/len(fix_quality)*100))

    print("Avg Distance from mean: %f" % average_distance_from_mean)
    print("Max Distance from mean: %f" % max_distance)    

    altitude_avg_distance_from_mean = (shifted_altitude - shifted_altitude_mean).abs().mean()
    print("Altitude mean: %f" % shifted_altitude_mean)
    print("Avg altitude distance from mean: %f" % altitude_avg_distance_from_mean)
    
    fig, ax = plt.subplots(2)
    
    print("Unique X values: %d" % len(pd.unique(shifted_utm_easting)))
    print("Unique Y values: %d" % len(pd.unique(shifted_utm_northing)))
    
    ax[0].plot(shifted_utm_easting.values, shifted_utm_northing.values, 'o', color='red')
    ax[0].plot(shifted_utm_easting_mean, shifted_utm_northing_mean, 'x', color='black')
    avg_distance_circle = plt.Circle((shifted_utm_easting_mean, shifted_utm_northing_mean), average_distance_from_mean, color='b', fill=False)
    ax[0].add_patch(avg_distance_circle)
    max_distance_circle = plt.Circle((shifted_utm_easting_mean, shifted_utm_northing_mean), max_distance, color='g', fill=False)
    ax[0].add_patch(max_distance_circle)

    ax[1].hist(distance_arr, bins = 25)
    ax[0].set_title("Stationary GPS UTM")
    ax[0].set_xlabel("UTM_Easting (Meters)")
    ax[0].set_ylabel("UTM_Northing (Meters")
    ax[1].set_title("Distance From Mean")
    ax[1].set_xlabel("Meters")
    ax[1].set_ylabel("Number of Points")
    plt.show()

