import rospy
import bagpy
from bagpy import bagreader
import pandas as pd
import seaborn as sea
import matplotlib.pyplot as plt
import numpy as np
from lab1.msg import GPSUTM
from sensor_msgs.msg import NavSatFix
import math



if __name__ == "__main__":

    bag_file_path = './movingisec.bag'
    gps_topic = '/gps/gps'
    bag = bagreader(bag_file_path)

    data = bag.message_by_topic(gps_topic)
    gps_df = pd.read_csv(data)

    raw_utm_easting = gps_df['easting']
    raw_utm_northing = gps_df['northing']
    raw_altitude = gps_df['alt']
    fix_quality = gps_df['fix']

    shifted_utm_easting = (raw_utm_easting - raw_utm_easting[0])
    shifted_utm_northing  = (raw_utm_northing - raw_utm_northing[0])
    shifted_altitude = raw_altitude# - raw_altitude[0]

    print("Percentage of Points with Fix: %f" % ((fix_quality == 4).sum()/len(fix_quality)*100))
    
    shifted_altitude_mean = shifted_altitude.mean()
    altitude_avg_distance_from_mean = (shifted_altitude - shifted_altitude_mean).abs().mean()
    print("Altitude mean: %f" % shifted_altitude_mean)
    print("Avg altitude distance from mean: %f" %altitude_avg_distance_from_mean)
    #isec
    square_corner_idx = [0,24,41,58]
    #roof
    #square_corner_idx = [2,18,36,52]
    x_square_vals = [[shifted_utm_easting.values[square_corner_idx[0]],shifted_utm_easting.values[square_corner_idx[1]]], [shifted_utm_easting.values[square_corner_idx[1]],shifted_utm_easting.values[square_corner_idx[2]]], [shifted_utm_easting.values[square_corner_idx[2]],shifted_utm_easting.values[square_corner_idx[3]]], [shifted_utm_easting.values[square_corner_idx[3]],shifted_utm_easting.values[square_corner_idx[0]]]]
    y_square_vals = [[shifted_utm_northing.values[square_corner_idx[0]],shifted_utm_northing.values[square_corner_idx[1]]], [shifted_utm_northing.values[square_corner_idx[1]],shifted_utm_northing.values[square_corner_idx[2]]], [shifted_utm_northing.values[square_corner_idx[2]],shifted_utm_northing.values[square_corner_idx[3]]], [shifted_utm_northing.values[square_corner_idx[3]],shifted_utm_northing.values[square_corner_idx[0]]]]
    square_side_length = []
    for i in range(4):
        square_side_length.append(round(math.sqrt((x_square_vals[i][1] - x_square_vals[i][0])**2+(y_square_vals[i][1] - y_square_vals[i][0])**2), 2))

    fig, ax = plt.subplots()#(2)
    ax.scatter(shifted_utm_easting.values, shifted_utm_northing.values, facecolors='none', edgecolors='r')
    ax.plot(x_square_vals[0], y_square_vals[0], label="length: " + str(square_side_length[0]))
    ax.plot(x_square_vals[1], y_square_vals[1], label="length: " + str(square_side_length[1]))
    ax.plot(x_square_vals[2], y_square_vals[2], label="length: " + str(square_side_length[2]))
    ax.plot(x_square_vals[3], y_square_vals[3], label="length: " + str(square_side_length[3]))
    leg = ax.legend()

    #ax[0].plot(shifted_utm_easting.values, m*shifted_utm_easting.values+b, '-b', label='line of best fit')    
    ax.set_title("Walking GPS UTM")
    ax.set_xlabel("UTM_Easting (Meters)")
    ax.set_ylabel("UTM_Northing (Meters)")
    #ax[1].hist(distance_to_best_fit_arr, bins = 25)    
    #ax[1].set_title("Distance From Best Fit Line")
    #ax[1].set_ylabel("Number of Points")
    #ax[1].set_xlabel("Meters")
    plt.show()


