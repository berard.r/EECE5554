import rospy
from bagpy import bagreader
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from lab1.msg import GPSUTM
from sensor_msgs.msg import NavSatFix
import math



if __name__ == "__main__":

    bag_file_path = '2022-02-28-17-47-29.bag'
    imu_topic = '/imu_data'
    magnetometer_topic = '/magnetometer_data'
    bag = bagreader(bag_file_path)

    imu_msg_data = bag.message_by_topic(imu_topic)
    
    imu_df = pd.read_csv(imu_msg_data)
    
    magnetometer_msg_data = bag.message_by_topic(magnetometer_topic) 
    magnetometer_df = pd.read_csv(magnetometer_msg_data)
    
    rostime = ((imu_df['header.stamp.secs'] - imu_df['header.stamp.secs'][0]).astype(str)+ ("." + (imu_df['header.stamp.nsecs']).astype(str))).astype(float)
    accel_x = imu_df['linear_acceleration.x']
    accel_y = imu_df['linear_acceleration.y']
    accel_z = imu_df['linear_acceleration.z']
    gyro_x = imu_df['angular_velocity.x']
    gyro_y = imu_df['angular_velocity.y']
    gyro_z = imu_df['angular_velocity.z']
    mag_x = magnetometer_df['magnetic_field.x']
    mag_y = magnetometer_df['magnetic_field.y']
    mag_z = magnetometer_df['magnetic_field.z']

        
    
    fig, ax = plt.subplots(3,3)
    
    print("accel_x std dev: %f" % accel_x.std())
    print("accel_x mean: %f" % accel_x.mean())
    print("accel_y std dev: %f" % accel_y.std())
    print("accel_y mean: %f" % accel_y.mean())
    print("accel_z std dev: %f" % accel_z.std())
    print("accel_z mean: %f\n" % accel_z.mean())

    print("gyro_x std dev: %f" % gyro_x.std())
    print("gyro_x mean: %f" % gyro_x.mean())
    print("gyro_y std dev: %f" % gyro_y.std())
    print("gyro_y mean: %f" % gyro_y.mean())
    print("gyro_z std dev: %f" % gyro_z.std())
    print("gyro_z mean: %f\n" % gyro_z.mean())

    print("mag_x std dev: %f" % mag_x.std())
    print("mag_x mean: %f" % mag_x.mean())
    print("mag_y std dev: %f" % mag_y.std())
    print("mag_y mean: %f" % mag_y.mean())
    print("mag_z std dev: %f" % mag_z.std())
    print("mag_z mean: %f" % mag_z.mean())


    ax[0,0].plot(rostime.values, accel_x.values)
    ax[0,1].plot(rostime.values, accel_y.values)
    ax[0,2].plot(rostime.values, accel_z.values)
    ax[1,0].plot(rostime.values, gyro_x.values)
    ax[1,1].plot(rostime.values, gyro_y.values)
    ax[1,2].plot(rostime.values, gyro_z.values)
    ax[2,0].plot(rostime.values, mag_x.values)
    ax[2,1].plot(rostime.values, mag_y.values)
    ax[2,2].plot(rostime.values, mag_z.values)
    
    #ax[0,0].set_title("X Accelertion vs. Time")
    ax[0,0].set_ylabel("X Acceleration (m/s)")
    ax[0,0].set_xlabel("Time (s)")
    #ax[0,1].set_title("Y Acceleration vs. Time")
    ax[0,1].set_ylabel("Y Acceleration (m/s)")
    ax[0,1].set_xlabel("Time (s)")
    #ax[0,2].set_title("Z Acceleration vs. Time")
    ax[0,2].set_ylabel("Z Acceleration (m/s)")
    ax[0,2].set_xlabel("Time (s)")

    #ax[1,0].set_title("X Angular Gyro vs. Time")
    ax[1,0].set_ylabel("X Angular Gyro (rad/s)")
    ax[1,0].set_xlabel("Time (s)")
    #ax[1,1].set_title("Y Angular Gyro vs. Time")
    ax[1,1].set_ylabel("Y Angular Gyro (rad/s)")
    ax[1,1].set_xlabel("Time (s)")
    #ax[1,2].set_title("Z Angular Gyro vs. Time")
    ax[1,2].set_ylabel("Z Angular Gyro (rad/s)")
    ax[1,2].set_xlabel("Time (s)")

    #ax[2,0].set_title("X Magnetometer vs. Time")
    ax[2,0].set_ylabel("X Magnetometer (Gauss)")
    ax[2,0].set_xlabel("Time (s)")
    #ax[2,1].set_title("Y Magnetometer vs. Time")
    ax[2,1].set_ylabel("Y Magnetometer (Gauss)")
    ax[2,1].set_xlabel("Time (s)")
    #ax[2,2].set_title("Z Magnetometer vs. Time")
    ax[2,2].set_ylabel("Z Magnetometer (Gauss)")
    ax[2,2].set_xlabel("Time (s)")

    ax[0,0].set_ylim([-.05, .2])
    ax[0,1].set_ylim([-.04, .08])
    ax[0,2].set_ylim([-9.1, -10.5])
    ax[1,0].set_ylim([-.01, .01])
    ax[1,1].set_ylim([-.01, .01])
    ax[1,2].set_ylim([-.01, .01])
    ax[2,0].set_ylim([.22, .275])
    ax[2,1].set_ylim([-.015, .02])
    ax[2,2].set_ylim([.28, .42])
    
    plt.show()
