#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
import serial
import math
import traceback
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField
import tf_conversions


class IMU_Driver:
    def __init__(self):
        try:

            self.imu_pub_topic = "/imu_data"
            self.magnetometer_pub_topic = "/magnetometer_data"
            self.usb_port = "/dev/ttyUSB2"
            self.baud_rate = 115200
            rospy.init_node("imu_driver")
            self.imu_pub = rospy.Publisher(
                self.imu_pub_topic, Imu, queue_size=10)
            self.imu_msg = Imu()
            
            self.magnetometer_pub = rospy.Publisher(
                self.magnetometer_pub_topic, MagneticField, queue_size=10)
            self.magnetometer_msg = MagneticField()

            self.imu_header = "$VNYMR"

            self.serial_connection = serial.Serial(
                self.usb_port, self.baud_rate, timeout=2)
            
            self.serial_connection.write("$VNWRG,07,40*XX".encode())

            self.imu_stream_handler()

        except Exception as e:
            error = traceback.format_exc()
            rospy.logerr("ERROR: %s" % error)

    def imu_stream_handler(self):
        rospy.loginfo("Starting imu stream")
        # loop unil package is shutdown
        while not rospy.is_shutdown():
            try:
            
                # read serial data
                line = self.serial_connection.readline()
                data = line.split(b',')
                data = [val.decode("utf-8") for val in data]
            


                if data[0][-6:]  == self.imu_header:  
                    
                        yaw = float(data[1])
                        pitch = float(data[2])
                        roll = float(data[3])
                        mag_x = float(data[4])
                        mag_y = float(data[5])
                        mag_z = float(data[6])
                        accel_x = float(data[7])
                        accel_y = float(data[8])
                        accel_z = float(data[9])
                        gyro_x = float(data[10])
                        gyro_y = float(data[11])
                        gyro_z = float(data[12][:-5])
                        rospy.loginfo('yaw: %.3f, pitch: %.3f, roll: %.3f' % (yaw, pitch, roll))
                        rostime = rospy.Time.now()
                        self.imu_msg.header.stamp = rostime

                        quaternion = tf_conversions.transformations.quaternion_from_euler(roll, pitch, yaw)

                        self.imu_msg.orientation.x = quaternion[0]
                        self.imu_msg.orientation.y = quaternion[1]
                        self.imu_msg.orientation.z = quaternion[2]
                        self.imu_msg.orientation.w = quaternion[3]
                        self.imu_msg.linear_acceleration.x = accel_x
                        self.imu_msg.linear_acceleration.y = accel_y
                        self.imu_msg.linear_acceleration.z = accel_z
                        self.imu_msg.angular_velocity.x = gyro_x
                        self.imu_msg.angular_velocity.y = gyro_y
                        self.imu_msg.angular_velocity.z = gyro_z

                        self.magnetometer_msg.header.stamp = rostime
                        self.magnetometer_msg.magnetic_field.x = mag_x
                        self.magnetometer_msg.magnetic_field.y = mag_y
                        self.magnetometer_msg.magnetic_field.z = mag_z


                        #rospy.loginfo("Yaw: %f  Pitch: %f Roll: %f" % (yaw, pitch, roll))
                        self.imu_pub.publish(self.imu_msg)
                        self.magnetometer_pub.publish(self.magnetometer_msg)
                    
            except:
                rospy.loginfo("IMU Driver: An exception has occured")
                pass



if __name__ == "__main__":

    imu_driver = IMU_Driver()