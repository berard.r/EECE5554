bag = rosbag('2022-03-10-17-54-24.bag');




circle_time = [bag.StartTime+((bag.EndTime-bag.StartTime)*.058), bag.StartTime+((bag.EndTime-bag.StartTime)*.111)];
gps_data = select(bag,"Time",[bag.StartTime, bag.EndTime],'Topic','/gps_utm');
imu_data = select(bag,"Time",[bag.StartTime, bag.EndTime],'Topic','/imu_data');
mag_data = select(bag,"Time",[bag.StartTime, bag.EndTime],'Topic','/magnetometer_data');

gps_data_circle = select(bag,"Time",circle_time,'Topic','/gps_utm');
mag_data_circle = select(bag,"Time",circle_time,'Topic','/magnetometer_data');

gps_struct = readMessages(gps_data,'DataFormat','struct');
imu_struct = readMessages(imu_data,'DataFormat','struct');
mag_struct = readMessages(mag_data,'DataFormat','struct'); 

accel_x = cellfun(@(m) double(m.LinearAcceleration.X),imu_struct);
accel_y = cellfun(@(m) double(m.LinearAcceleration.Y),imu_struct);
accel_z = cellfun(@(m) double(m.LinearAcceleration.Z),imu_struct);

gps_x = cellfun(@(m) double(m.UtmEasting),gps_struct);
gps_y = cellfun(@(m) double(m.UtmNorthing),gps_struct);

mag_struct_circle = readMessages(mag_data_circle,'DataFormat','struct');
gps_struct_circle = readMessages(gps_data_circle,'DataFormat','struct');

gyro_z = cellfun(@(m) double(m.AngularVelocity.Z),imu_struct);

mag_x = cellfun(@(m) double(m.MagneticField_.X),mag_struct);
mag_y = cellfun(@(m) double(m.MagneticField_.Y),mag_struct);
mag_z = cellfun(@(m) double(m.MagneticField_.Z),mag_struct);

mag_circle_x = cellfun(@(m) double(m.MagneticField_.X),mag_struct_circle);
mag_circle_y = cellfun(@(m) double(m.MagneticField_.Y),mag_struct_circle);
mag_circle_z = cellfun(@(m) double(m.MagneticField_.Z),mag_struct_circle);

gps_circle_x = cellfun(@(m) double(m.UtmEasting),gps_struct_circle);
gps_circle_y = cellfun(@(m) double(m.UtmNorthing),gps_struct_circle);

imu_rate = 40;

imu_quat_x = cellfun(@(m) double(m.Orientation.X),imu_struct);
imu_quat_y = cellfun(@(m) double(m.Orientation.Y),imu_struct);
imu_quat_z = cellfun(@(m) double(m.Orientation.Z),imu_struct);
imu_quat_w = cellfun(@(m) double(m.Orientation.W),imu_struct);

ypr = quat2eul([imu_quat_w, imu_quat_x, imu_quat_y, imu_quat_z]);
yaw = ypr(:,1);
yaw_unwrapped = deg2rad(unwrap(yaw));
yaw_wrapped = wrapTo2Pi(yaw_unwrapped);
figure
plot(yaw_wrapped)

D = [mag_circle_x, mag_circle_y, mag_circle_z];
D_all = [mag_x, mag_y, mag_z];
[A,b,expmfs] = magcal(D);
C = (D-b)*A;
mag_data_calibrated = ((D_all-b)*A);

figure;
hold on
title("Uncorrected vs. Corrected mag readings")
a1 = plot(C(:,1), C(:,2)); M1 = "Corrected mag readings";
a2 = plot(mag_circle_x, mag_circle_y); M2= "Uncorrected mag readings";
legend([a1, a2], [M1, M2]);


imu_time = 0:(1/imu_rate):(length(gyro_z)-1)*(1/imu_rate);
imu_time = imu_time.';
gps_time = 0:1:length(gps_x)-2;

gyro_z_integrated = cumtrapz(imu_time, gyro_z);

figure
plot(imu_time, gyro_z_integrated)
gyro_z_integrated_2pi = wrapTo2Pi(gyro_z_integrated);

figure
plot(imu_time, gyro_z_integrated_2pi)
title("gyro yaw")


mag_yaw = unwrap(atan2(-mag_data_calibrated_y, mag_data_calibrated_x));

mag_yaw_2pi = wrapTo2Pi(mag_yaw);

figure
plot(mag_yaw)
title("mag yaw")

figure
plot(imu_time, mag_yaw_2pi)
title("mag yaw 2pi")

figure;
hold on
title("Magnetometer Yaw vs. Gyro Yaw")
xlabel("Time (s)")
ylabel("Yaw (rads)")
a1 = plot(imu_time, mag_yaw); M1 = "Magnetometer Yaw";
a2 = plot(imu_time, gyro_z_integrated); M2= "Gyro Yaw";
legend([a1, a2], [M1, M2]);


alpha = .1;
complementary=wrapToPi((alpha*mag_yaw)+((1-alpha)*wrapToPi(gyro_z_integrated)));
%complementary = wrapTo2Pi(complementary);
complementary = unwrap(complementary);
figure
plot(complementary)
title("complementary yaw")


figure;
hold on
title("Complementary Filter Yaw vs. IMU Yaw")
xlabel("Time (s)")
ylabel("Yaw (rads)")
a1 = plot(imu_time, complementary); M1 = "Complementary Filter Yaw";
a2 = plot(imu_time, yaw_unwrapped); M2= "IMU Yaw";
legend([a1, a2], [M1, M2]);


accel_x_mean = mean(accel_x);
corrected_accel_x = accel_x - accel_x_mean;

imu_velocity = cumtrapz(imu_time, accel_x);

figure
plot(imu_time, imu_velocity)
title("Uncorrected Velocity")
xlabel("Time (s)")
ylabel("Velocity (m/s)")

accel_x_polyfit = polyfit((1:length(corrected_accel_x)).', corrected_accel_x, 5);

corrected_accel_x = corrected_accel_x - polyval(accel_x_polyfit, 1:length(corrected_accel_x)).';

imu_velocity = cumtrapz(imu_time, corrected_accel_x);

figure
plot(imu_time, imu_velocity)
title("Corrected Velocity")
xlabel("Time (s)")
ylabel("Velocity (m/s)")

diff_gps_x = diff(gps_x);
diff_gps_y = diff(gps_y);
gps_velocity = hypot(diff_gps_x, diff_gps_y);

figure
plot(gps_time, gps_velocity)
title("GPS Velocity")
xlabel("Time (s)")
ylabel("Velocity (m/s)")

wX_gyro = imu_velocity .* gyro_z;
figure

plot(imu_time, wX_gyro, 'b')
title("Y Accel. Calculated")
xlabel("Time (s)")
ylabel("Acceleration (m/s^2)")
figure

plot(imu_time, (accel_y), 'r')
title("Y Accel. Observed")
xlabel("Time (s)")
ylabel("Acceleration (m/s^2)")

ve = imu_velocity .* cos(mag_yaw_2pi);
vn = imu_velocity .* sin(mag_yaw_2pi);
xe = cumtrapz(imu_time, ve);
xn = cumtrapz(imu_time, vn);

scale = .2;
rot = deg2rad(40);

new_xe = (xe*scale)*cos(rot) - (xn*scale)*sin(rot);
new_xn = (xn*scale)*cos(rot) + (xn*scale)*sin(rot);

figure
plot(new_xe, new_xn, 'b')
hold on;
plot((gps_x-gps_x(1)), gps_y-gps_y(1), 'r')
xlabel("Easting (m)")
ylabel("Northing (m)")

a1 = plot(imu_time, complementary); M1 = "Complementary Filter Yaw";
a2 = plot(imu_time, yaw_unwrapped); M2= "IMU Yaw";
legend([a1, a2], [M1, M2]);
