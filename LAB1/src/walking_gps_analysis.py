import rospy
import bagpy
from bagpy import bagreader
import pandas as pd
import seaborn as sea
import matplotlib.pyplot as plt
import numpy as np
from lab1.msg import GPSUTM
from sensor_msgs.msg import NavSatFix
import math



if __name__ == "__main__":

    bag_file_path = './data/2022-02-08-13-31-32.bag'
    gps_topic = '/gps_utm'
    bag = bagreader(bag_file_path)

    data = bag.message_by_topic(gps_topic)
    gps_df = pd.read_csv(data)


    raw_utm_easting = gps_df['utm_easting']
    raw_utm_northing = gps_df['utm_northing']

    raw_altitude = gps_df['altitude']

    shifted_utm_easting = (raw_utm_easting - raw_utm_easting[0])
    shifted_utm_northing  = (raw_utm_northing - raw_utm_northing[0])
    shifted_altitude = raw_altitude# - raw_altitude[0]

    m,b = np.polyfit(shifted_utm_easting.values, shifted_utm_northing.values, 1)

    distance_to_best_fit_arr = []
    for idx, val in shifted_utm_easting.items():
        distance_to_best_fit = abs((m * shifted_utm_easting[idx] + (-1) * shifted_utm_northing[idx] + b)) / (math.sqrt(m * m + (-1) * (-1)))
        distance_to_best_fit_arr.append(distance_to_best_fit)


    distance_to_best_fit_arr = np.asarray(distance_to_best_fit_arr)
    distance_to_best_fit_average = distance_to_best_fit_arr.mean()
    max_distance_to_best_fit = distance_to_best_fit_arr.max()

    print(distance_to_best_fit_average)
    print(max_distance_to_best_fit)

    shifted_altitude_mean = shifted_altitude.mean()
    altitude_avg_distance_from_mean = (shifted_altitude - shifted_altitude_mean).abs().mean()
    print(shifted_altitude_mean)
    print(altitude_avg_distance_from_mean)

    fig, ax = plt.subplots(2)
    ax[0].plot(shifted_utm_easting.values, shifted_utm_northing.values, 'o', color='red')
    ax[0].plot(shifted_utm_easting.values, m*shifted_utm_easting.values+b, '-b', label='line of best fit')    
    ax[0].set_title("Walking GPS UTM")
    ax[0].set_xlabel("UTM_Easting (Meters)")
    ax[0].set_ylabel("UTM_Northing (Meters)")
    ax[1].hist(distance_to_best_fit_arr, bins = 25)    
    ax[1].set_title("Distance From Best Fit Line")
    ax[1].set_ylabel("Number of Points")
    ax[1].set_xlabel("Meters")
    plt.show()


