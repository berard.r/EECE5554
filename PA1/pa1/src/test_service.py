#!/usr/bin/env python


from rospy_tutorials.srv import *
import rospy

def add_ints(srv):
    result = srv.a + srv.b
    print('service called')
    return result

def add_two_ints_server():
    rospy.init_node('test_service')
    ser = rospy.Service('add_ints', AddTwoInts, add_ints)
    
    rospy.spin()

if __name__ == "__main__":
    add_two_ints_server()
