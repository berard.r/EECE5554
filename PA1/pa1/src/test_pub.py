#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64

def publish_test():
    pub = rospy.Publisher('topic', Float64, queue_size=2)
    rospy.init_node('test_pub', anonymous=True)
    rate = rospy.Rate(10) 
    i = 0
    while not rospy.is_shutdown():

        pub.publish(i)
        i += 1
        print(i)
        rate.sleep()

if __name__ == '__main__':
    
    publish_test()
    