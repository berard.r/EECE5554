#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64

def test_callback(msg_data):
    rospy.loginfo("msg: %d" % msg_data.data)
    print(msg_data.data)
    
def subscribe_test():

    
    rospy.init_node('test_sub', anonymous=True)

    rospy.Subscriber('topic', Float64, test_callback)

    rospy.spin()

if __name__ == '__main__':
    subscribe_test()