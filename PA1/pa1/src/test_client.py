#!/usr/bin/env python


import sys
import rospy
from rospy_tutorials.srv import *

def test_client(a, b):
    rospy.wait_for_service('add_ints')
    
    add_ints = rospy.ServiceProxy('add_ints', AddTwoInts)
    result = add_ints(a, b)
    return result.sum


def usage():
    return "%s [a b]"%sys.argv[0]

if __name__ == "__main__":
    if len(sys.argv) == 3:
        a = int(sys.argv[1])
        b = int(sys.argv[2])
    else:
        print(usage())
        sys.exit(1)
    
    print("%s + %s = %s"%(a, b, test_client(a, b)))
